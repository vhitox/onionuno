package bo.gob.sin.empty.application;

import bo.gob.sin.empty.application.impl.RespuestaGuardadoChiste;

import java.util.concurrent.CompletableFuture;

public interface IGuardaChisteService {
    RespuestaGuardadoChiste guardaElChisteChuck();
    CompletableFuture<RespuestaGuardadoChiste> guardaElChisteChuckAsync();
}
