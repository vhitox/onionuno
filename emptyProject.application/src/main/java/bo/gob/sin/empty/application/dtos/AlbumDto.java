package bo.gob.sin.empty.application.dtos;

import bo.gob.sin.sen.onion.domain.models.dtos.ModelDto;

public class AlbumDto extends ModelDto {
    private Integer userId;
    private Integer id;
    private String title;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static AlbumDtoBuilder builder() {
        return new AlbumDtoBuilder();
    }

    public static final class AlbumDtoBuilder {
        private AlbumDto albumDto;

        private AlbumDtoBuilder() {
            albumDto = new AlbumDto();
        }

        public AlbumDtoBuilder userId(Integer userId) {
            albumDto.setUserId(userId);
            return this;
        }

        public AlbumDtoBuilder id(Integer id) {
            albumDto.setId(id);
            return this;
        }

        public AlbumDtoBuilder title(String title) {
            albumDto.setTitle(title);
            return this;
        }

        public AlbumDto build() {
            return albumDto;
        }
    }
}
