package bo.gob.sin.empty.application.impl;

import bo.gob.sin.sen.onion.application.impl.RespuestaServicio;
import bo.gob.sin.sen.onion.domain.MensajeServicio;

import java.util.List;

public class RespuestaCkuck extends RespuestaServicio {
    private String categories[];
    private String created_at;
    private String icon_url;
    private String id;
    private String updated_at;
    private String url;
    private String value;

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static RespuestaCkuckBuilder builder() {
        return new RespuestaCkuckBuilder();
    }

    public static final class RespuestaCkuckBuilder {
        private RespuestaCkuck respuestaCkuck;

        private RespuestaCkuckBuilder() {
            respuestaCkuck = new RespuestaCkuck();
        }

        public RespuestaCkuckBuilder categories(String[] categories) {
            respuestaCkuck.setCategories(categories);
            return this;
        }

        public RespuestaCkuckBuilder created_at(String created_at) {
            respuestaCkuck.setCreated_at(created_at);
            return this;
        }

        public RespuestaCkuckBuilder icon_url(String icon_url) {
            respuestaCkuck.setIcon_url(icon_url);
            return this;
        }

        public RespuestaCkuckBuilder id(String id) {
            respuestaCkuck.setId(id);
            return this;
        }

        public RespuestaCkuckBuilder updated_at(String updated_at) {
            respuestaCkuck.setUpdated_at(updated_at);
            return this;
        }

        public RespuestaCkuckBuilder url(String url) {
            respuestaCkuck.setUrl(url);
            return this;
        }

        public RespuestaCkuckBuilder value(String value) {
            respuestaCkuck.setValue(value);
            return this;
        }

        public RespuestaCkuckBuilder transaccion(Boolean transaccion) {
            respuestaCkuck.setTransaccion(transaccion);
            return this;
        }

        public RespuestaCkuckBuilder mensajes(List<MensajeServicio> mensajes) {
            respuestaCkuck.setMensajes(mensajes);
            return this;
        }

        public RespuestaCkuck build() {
            return respuestaCkuck;
        }
    }
}
