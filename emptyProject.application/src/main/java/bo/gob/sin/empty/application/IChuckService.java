package bo.gob.sin.empty.application;

import bo.gob.sin.empty.application.impl.RespuestaCkuck;
import bo.gob.sin.sen.onion.application.IApplicationService;

import java.util.concurrent.CompletableFuture;

public interface IChuckService extends IApplicationService {
    RespuestaCkuck obtenerChiste();
    CompletableFuture<RespuestaCkuck> obtenerChisteAsync();
}
