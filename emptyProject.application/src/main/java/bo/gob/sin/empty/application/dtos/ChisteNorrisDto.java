package bo.gob.sin.empty.application.dtos;

import bo.gob.sin.sen.onion.domain.models.dtos.ModelDto;

public class ChisteNorrisDto extends ModelDto {

    private String value;
    private String created_at;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCreated_at() {
        return created_at;
    }


    public static ChisteNorrisDtoBuilder builder() {
        return new ChisteNorrisDtoBuilder();
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public static final class ChisteNorrisDtoBuilder {
        private ChisteNorrisDto chisteNorrisDto;

        private ChisteNorrisDtoBuilder() {
            chisteNorrisDto = new ChisteNorrisDto();
        }



        public ChisteNorrisDtoBuilder value(String value) {
            chisteNorrisDto.setValue(value);
            return this;
        }

        public ChisteNorrisDtoBuilder created_at(String created_at) {
            chisteNorrisDto.setCreated_at(created_at);
            return this;
        }

        public ChisteNorrisDto build() {
            return chisteNorrisDto;
        }
    }
}
