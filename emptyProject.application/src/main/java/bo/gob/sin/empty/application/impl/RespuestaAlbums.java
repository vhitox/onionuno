package bo.gob.sin.empty.application.impl;

import bo.gob.sin.empty.application.dtos.AlbumDto;
import bo.gob.sin.sen.onion.application.impl.RespuestaServicio;
import bo.gob.sin.sen.onion.domain.MensajeServicio;

import java.util.ArrayList;
import java.util.List;

public class RespuestaAlbums{
    private List<AlbumDto> data;

    public List<AlbumDto> getData() {
        return data;
    }

    public void setData(List<AlbumDto> data) {
        this.data = data;
    }

    public static RespuestaAlbumsBuilder builder() {
        return new RespuestaAlbumsBuilder();
    }

    public static final class RespuestaAlbumsBuilder {
        private RespuestaAlbums respuestaAlbums;

        private RespuestaAlbumsBuilder() {
            respuestaAlbums = new RespuestaAlbums();
        }

        public RespuestaAlbumsBuilder data(List<AlbumDto> data) {
            respuestaAlbums.setData(data);
            return this;
        }

       /* public RespuestaAlbumsBuilder transaccion(Boolean transaccion) {
            respuestaAlbums.setTransaccion(transaccion);
            return this;
        }

        public RespuestaAlbumsBuilder mensajes(List<MensajeServicio> mensajes) {
            respuestaAlbums.setMensajes(mensajes);
            return this;
        }*/

        public RespuestaAlbums build() {
            return respuestaAlbums;
        }
    }
}
