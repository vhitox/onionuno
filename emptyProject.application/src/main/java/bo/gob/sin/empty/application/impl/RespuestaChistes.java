package bo.gob.sin.empty.application.impl;

import bo.gob.sin.empty.application.dtos.ChisteNorrisDto;
import bo.gob.sin.sen.onion.application.impl.RespuestaServicio;
import bo.gob.sin.sen.onion.domain.MensajeServicio;

import java.util.List;

public class RespuestaChistes extends RespuestaServicio {

    private List<ChisteNorrisDto> data;

    public List<ChisteNorrisDto> getData() {
        return data;
    }

    public void setData(List<ChisteNorrisDto> data) {
        this.data = data;
    }

    public static RespuestaChistesBuilder builder() {
        return new RespuestaChistesBuilder();
    }

    public static final class RespuestaChistesBuilder {
        private RespuestaChistes respuestaChistes;

        private RespuestaChistesBuilder() {
            respuestaChistes = new RespuestaChistes();
        }



        public RespuestaChistesBuilder data(List<ChisteNorrisDto> data) {
            respuestaChistes.setData(data);
            return this;
        }

        public RespuestaChistesBuilder transaccion(Boolean transaccion) {
            respuestaChistes.setTransaccion(transaccion);
            return this;
        }

        public RespuestaChistesBuilder mensajes(List<MensajeServicio> mensajes) {
            respuestaChistes.setMensajes(mensajes);
            return this;
        }

        public RespuestaChistes build() {
            return respuestaChistes;
        }
    }
}
