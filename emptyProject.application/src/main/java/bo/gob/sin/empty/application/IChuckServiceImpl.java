package bo.gob.sin.empty.application;

import bo.gob.sin.empty.application.dtos.ChisteNorrisDto;
import bo.gob.sin.empty.application.impl.RespuestaChistes;
import bo.gob.sin.empty.application.impl.RespuestaCkuck;
import bo.gob.sin.empty.application.impl.RespuestaUnChiste;

import java.util.concurrent.CompletableFuture;

public interface IChuckServiceImpl {

    RespuestaChistes getChistes(Integer chistes);
    CompletableFuture<RespuestaChistes> getChistesAsync(Integer chistes);

    //Servicio para obtener un solo chiste

    RespuestaUnChiste hazmeReir();
    CompletableFuture<RespuestaUnChiste> hazmeReirAsync();


}
