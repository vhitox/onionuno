package bo.gob.sin.empty.application.impl;

import bo.gob.sin.sen.onion.application.impl.RespuestaServicio;
import bo.gob.sin.sen.onion.domain.MensajeServicio;

import java.util.List;

public class RespuestaUnChiste extends RespuestaServicio {

    private RespuestaCkuck chiste;

    public RespuestaCkuck getChiste() {
        return chiste;
    }

    public void setChiste(RespuestaCkuck chiste) {
        this.chiste = chiste;
    }

    public static RespuestaUnChisteBuilder builder() {
        return new RespuestaUnChisteBuilder();
    }

    public static final class RespuestaUnChisteBuilder {
        private RespuestaUnChiste respuestaUnChiste;

        private RespuestaUnChisteBuilder() {
            respuestaUnChiste = new RespuestaUnChiste();
        }

        public RespuestaUnChisteBuilder chiste(RespuestaCkuck chiste) {
            respuestaUnChiste.setChiste(chiste);
            return this;
        }

        public RespuestaUnChisteBuilder transaccion(Boolean transaccion) {
            respuestaUnChiste.setTransaccion(transaccion);
            return this;
        }

        public RespuestaUnChisteBuilder mensajes(List<MensajeServicio> mensajes) {
            respuestaUnChiste.setMensajes(mensajes);
            return this;
        }

        public RespuestaUnChiste build() {
            return respuestaUnChiste;
        }
    }
}
