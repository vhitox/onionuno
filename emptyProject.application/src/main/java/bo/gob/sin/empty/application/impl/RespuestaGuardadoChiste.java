package bo.gob.sin.empty.application.impl;

import bo.gob.sin.sen.onion.application.impl.RespuestaServicio;
import bo.gob.sin.sen.onion.domain.MensajeServicio;

import java.util.List;

public class RespuestaGuardadoChiste extends RespuestaServicio {

    public static RespuestaGuardadoChisteBuilder builder() {
        return new RespuestaGuardadoChisteBuilder();
    }
    public static final class RespuestaGuardadoChisteBuilder {
        private RespuestaGuardadoChiste respuestaGuardadoChiste;

        private RespuestaGuardadoChisteBuilder() {
            respuestaGuardadoChiste = new RespuestaGuardadoChiste();
        }

        public RespuestaGuardadoChisteBuilder transaccion(Boolean transaccion) {
            respuestaGuardadoChiste.setTransaccion(transaccion);
            return this;
        }

        public RespuestaGuardadoChisteBuilder mensajes(List<MensajeServicio> mensajes) {
            respuestaGuardadoChiste.setMensajes(mensajes);
            return this;
        }

        public RespuestaGuardadoChiste build() {
            return respuestaGuardadoChiste;
        }
    }
}
