package bo.gob.sin.empty.application;

import bo.gob.sin.empty.application.dtos.AlbumDto;
import bo.gob.sin.empty.application.impl.RespuestaAlbums;
import bo.gob.sin.sen.onion.application.IApplicationService;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface IAlbumService extends IApplicationService {
    AlbumDto obtenerAlbum(Integer pId);
    CompletableFuture<AlbumDto> obtenerAlbumAsync(Integer pId);

    AlbumDto[] obtenerListadoALbums();
    CompletableFuture<AlbumDto[]> obtenerListadoALbumsAsync();

    List<AlbumDto>  obtenerListadoALbums2();
    CompletableFuture<List<AlbumDto>> obtenerListadoALbumsAsync2();

}
