package bo.gob.sin.empty.infraestructure.rest;


import bo.gob.sin.empty.application.services.ApplicationServiceFactory;
import bo.gob.sin.sen.onion.domain.models.converters.StringConverter;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;
import spark.Route;

public class ChuckController {
    public static Route getJoke = (Request request, Response response) -> {
        try{
            var vServicio = ApplicationServiceFactory.getJokeService();
            var vChistes = StringConverter.toInteger().convert(request.params("id"));
            return vServicio.getChistesAsync(vChistes).join();

        }catch (Exception ex){
            ex.printStackTrace();
            response.status(HttpStatus.BAD_REQUEST_400);
        }
        return null;
    };

    public static Route almacenaChiste = (Request request, Response response) -> {
        try{
            var vServicio = ApplicationServiceFactory.guardaChisteService();
            return vServicio.guardaElChisteChuck();

        }catch (Exception ex){
            ex.printStackTrace();
            response.status(HttpStatus.BAD_REQUEST_400);
        }
        return null;
    };
}
