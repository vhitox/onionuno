package bo.gob.sin.empty.infraestructure.rest;

import bo.gob.sin.sen.onion.domain.AppPropertiesSingle;
import bo.gob.sin.sen.onion.domain.services.postgres.PostgresDataSourceSingle;
import bo.gob.sin.sen.onion.infraestructure.rest.AfterFilters;
import bo.gob.sin.sen.onion.infraestructure.rest.BeforeFilters;
import bo.gob.sin.sen.onion.infraestructure.rest.JsonTransformer;

import java.time.LocalDate;

import static spark.Spark.*;
import static spark.Spark.after;

public class ChuckApi {
    public static void main(String[] args) {
        port(Integer.parseInt(AppPropertiesSingle.getString("server.port")));
        int maxThreads = Integer.parseInt(AppPropertiesSingle.getString("server.maxThreads"));
        int minThreads = Integer.parseInt(AppPropertiesSingle.getString("server.minThreads"));
        int timeOutMillis = Integer.parseInt(AppPropertiesSingle.getString("server.timeOutMillis"));
        threadPool(maxThreads, minThreads, timeOutMillis);

        before("/rest/*", BeforeFilters.initResponse, BeforeFilters.validateRequest);
        after(AfterFilters.corsResponse);

        PostgresDataSourceSingle.create();
        path("/", () -> {
            get("obtenerChistes/:id", ChuckController.getJoke, JsonTransformer.getInstance());
            get("guardaChiste", ChuckController.almacenaChiste, JsonTransformer.getInstance());
        });
        awaitInitialization();
        System.out.println(String.format("%d: demo_rest,  fecha: %s", port(), LocalDate.now()));
    }
}
