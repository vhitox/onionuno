package bo.gob.sin.empty.application.services.clients;

import bo.gob.sin.empty.application.IAlbumService;
import junit.framework.TestCase;

public class AlbumServiceClientTest extends TestCase {

    private IAlbumService client = ServiceClientFactory.instance("http://localhost:3000").getAlbumsList();
    public void testObtenerAlbums() {
        var vResultado = client.obtenerAlbum(10);
        System.out.println(vResultado);
    }

    public void testObtenerListadoALbums() {
        var vResultado = client.obtenerListadoALbums();
        System.out.println(vResultado[0]);
    }

    public void testObtenerListadoALbums2() {
        var vResultado = client.obtenerListadoALbums2();
        System.out.println(vResultado);
    }
}