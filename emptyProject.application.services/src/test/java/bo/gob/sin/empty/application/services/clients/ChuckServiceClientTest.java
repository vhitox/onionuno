package bo.gob.sin.empty.application.services.clients;

import bo.gob.sin.empty.application.IChuckService;
import junit.framework.TestCase;

public class ChuckServiceClientTest extends TestCase {

    /*private IChuckService client = ServiceClientFactory.instance("https://api.chucknorris.io/jokes/random").getChuckJokes();*/
    private IChuckService client = ServiceClientFactory.getInstance().getChuckJokes();
    public void testObtenerChiste() {
        var vResultado = client.obtenerChiste();
        System.out.println(vResultado);
    }
}