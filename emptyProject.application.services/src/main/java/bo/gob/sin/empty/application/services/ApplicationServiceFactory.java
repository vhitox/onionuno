package bo.gob.sin.empty.application.services;

import bo.gob.sin.empty.application.IChuckServiceImpl;
import bo.gob.sin.empty.application.IGuardaChisteService;
import bo.gob.sin.empty.application.services.clients.ServiceClientFactory;
import bo.gob.sin.empty.application.services.impl.ChuckServiceImpl;
import bo.gob.sin.empty.application.services.impl.GuardaChisteServiceImpl;
import bo.gob.sin.empty.domain.respositories.IChuckRepository;
import bo.gob.sin.empty.domain.services.DomainServiceFactory;
import bo.gob.sin.empty.domain.services.repositories.ChuckRepositoryImpl;

public class ApplicationServiceFactory {

    public static IChuckServiceImpl getJokeService() {
        return new ChuckServiceImpl(ServiceClientFactory.getInstance().getChuckJokes());
    }

    public static IGuardaChisteService guardaChisteService(){
        return new GuardaChisteServiceImpl(ServiceClientFactory.getInstance().getChuckJokes(), DomainServiceFactory.adicionarChiste());
    }
}
