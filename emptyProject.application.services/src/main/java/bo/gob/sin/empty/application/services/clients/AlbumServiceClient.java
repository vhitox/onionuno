package bo.gob.sin.empty.application.services.clients;

import bo.gob.sin.empty.application.IAlbumService;
import bo.gob.sin.empty.application.dtos.AlbumDto;
import bo.gob.sin.empty.application.impl.RespuestaAlbums;
import bo.gob.sin.sen.onion.application.service.clients.ClientServiceImpl;
import bo.gob.sin.sen.onion.infraestructure.rest.JsonTransformer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.modelmapper.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class AlbumServiceClient extends ClientServiceImpl implements IAlbumService {

    private  static final GsonBuilder gsonBuilder = JsonTransformer.getInstance().getGsonBuilder();

    private String urlBase;
    public AlbumServiceClient(){ this.urlBase = "http://localhost:3000" ;}
    public AlbumServiceClient(String urlBase){
        super();
        this.urlBase = urlBase;
    }

    @Override
    public AlbumDto obtenerAlbum(Integer pId) {
        return obtenerAlbumAsync(pId).join();
    }

    @Override
    public CompletableFuture<AlbumDto> obtenerAlbumAsync(Integer pId) {
        var vUrl = new StringBuilder(urlBase);
        vUrl.append("/albums/");
        vUrl.append(pId);
        System.out.println(vUrl);
        return getAsyncRestClient()
                .getAsync(vUrl.toString(), jsonBuilder.toJson(""))
                .thenApply(vResponse -> {
                    System.out.println(vResponse);
                    var rep = jsonBuilder.fromJson(vResponse, AlbumDto.class);
                    return rep;

                });
    }

    @Override
    public AlbumDto[] obtenerListadoALbums() {
        return obtenerListadoALbumsAsync().join();
    }

    @Override
    public CompletableFuture<AlbumDto[]> obtenerListadoALbumsAsync() {
        var vUrl = new StringBuilder(urlBase);
        vUrl.append("/albums");
        System.out.println(vUrl);
        List<AlbumDto> albums;
        return getAsyncRestClient()
                .getAsync(vUrl.toString(), jsonBuilder.toJson(""))
                .thenApply(vResponse -> {
                   return jsonBuilder.fromJson(vResponse, AlbumDto[].class) ;
                });
    }

    @Override
    public List<AlbumDto> obtenerListadoALbums2() {
        return obtenerListadoALbumsAsync2().join();
    }

    @Override
    public CompletableFuture<List<AlbumDto>> obtenerListadoALbumsAsync2() {
        Type founderListType = new TypeToken<ArrayList<AlbumDto>>(){}.getType();
        var vUrl = new StringBuilder(urlBase);
        vUrl.append("/albums");
        System.out.println(vUrl);
        List<AlbumDto> albums;
        return getAsyncRestClient()
                .getAsync(vUrl.toString())
                .thenApply(vResponse -> {
                    return gsonBuilder.create().fromJson(vResponse, founderListType) ;
                });
    }
}
