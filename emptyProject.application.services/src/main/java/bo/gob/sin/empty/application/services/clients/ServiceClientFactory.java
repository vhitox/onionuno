package bo.gob.sin.empty.application.services.clients;

import bo.gob.sin.sen.onion.application.clients.ConfigPropertiesSingle;

public class ServiceClientFactory {
    private static ServiceClientFactory instance;
    private static String url;

    public static ServiceClientFactory instance(String pUrl) {
        if(instance == null){
            url = pUrl;
            instance = new ServiceClientFactory();
        }
        return instance;
    }

    public static ServiceClientFactory getInstance(){
        if(instance == null){
            instance(ConfigPropertiesSingle.getProperty("config_entorno_url_chuck"));
        }
        return instance;
    }

    public ChuckServiceClient getChuckJokes() {
        return new ChuckServiceClient(url);
    }
    public AlbumServiceClient getAlbumsList() { return new AlbumServiceClient(url); }
}
