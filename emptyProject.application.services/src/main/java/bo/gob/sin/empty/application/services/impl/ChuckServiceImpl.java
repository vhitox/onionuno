package bo.gob.sin.empty.application.services.impl;

import bo.gob.sin.empty.application.IChuckService;
import bo.gob.sin.empty.application.IChuckServiceImpl;
import bo.gob.sin.empty.application.dtos.ChisteNorrisDto;
import bo.gob.sin.empty.application.impl.RespuestaChistes;
import bo.gob.sin.empty.application.impl.RespuestaCkuck;
import bo.gob.sin.empty.application.impl.RespuestaUnChiste;
import bo.gob.sin.empty.application.services.ApplicationServiceFactory;
import bo.gob.sin.sen.onion.application.FixedExecutorSingle;
import bo.gob.sin.sen.onion.application.service.ApplicationServiceImpl;
import bo.gob.sin.sen.onion.domain.MensajeServicioBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class ChuckServiceImpl extends ApplicationServiceImpl implements IChuckServiceImpl {

    private IChuckService client;

    public ChuckServiceImpl(IChuckService client){
        this.client = client;
    }

    @Override
    public RespuestaChistes getChistes(Integer chistes) {
        ArrayList<ChisteNorrisDto> lista = new ArrayList<>();
        System.out.println("CLiente "+client);
        var vRespuesta = RespuestaChistes.builder().transaccion(Boolean.FALSE);

        try{
            for (int i = 1; i <= chistes ; i++) {
                var vChiste = client.obtenerChiste();
                var vRespuestaChiste = ChisteNorrisDto.builder()
                        .value(vChiste.getValue())
                        .created_at(vChiste.getCreated_at())
                        .build();
                lista.add(vRespuestaChiste);
            }
            vRespuesta.transaccion(Boolean.TRUE);
            vRespuesta.data(lista).build();
        } catch (Exception ex){
            ex.printStackTrace();
            var vMensaje = "Ocurrio un error al obtener los chistes";
            vRespuesta.build().addMensaje(MensajeServicioBuilder.create().codigo(-1).descripcion(vMensaje).build());
        }
        return vRespuesta.build();
    }

    @Override
    public CompletableFuture<RespuestaChistes> getChistesAsync(Integer chistes) {
        return CompletableFuture.supplyAsync(() -> getChistes(chistes), FixedExecutorSingle.instance());
    }

    @Override
    public RespuestaUnChiste hazmeReir() {
        var vRespuesta = RespuestaUnChiste.builder().transaccion(Boolean.FALSE);
        try{
            var vChiste = client.obtenerChiste();
            vRespuesta.chiste(vChiste);
            vRespuesta.transaccion(Boolean.TRUE);
            System.out.println("vRespuesta : "+vRespuesta);
        }catch (Exception ex){
            ex.printStackTrace();
            var vMensaje = "No me hizo reir";
            System.out.println(vMensaje);
        }
        return vRespuesta.build();
    }

    @Override
    public CompletableFuture<RespuestaUnChiste> hazmeReirAsync() {
        return CompletableFuture.supplyAsync(()-> hazmeReir(), FixedExecutorSingle.instance());
    }
}
