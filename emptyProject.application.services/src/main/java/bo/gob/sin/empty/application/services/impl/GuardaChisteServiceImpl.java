package bo.gob.sin.empty.application.services.impl;

import bo.gob.sin.empty.application.IChuckService;
import bo.gob.sin.empty.application.IChuckServiceImpl;
import bo.gob.sin.empty.application.IGuardaChisteService;
import bo.gob.sin.empty.application.impl.RespuestaGuardadoChiste;
import bo.gob.sin.empty.application.services.clients.ChuckServiceClient;
import bo.gob.sin.empty.domain.respositories.IChuckRepository;
import bo.gob.sin.sen.onion.application.FixedExecutorSingle;
import bo.gob.sin.sen.onion.application.service.ApplicationServiceImpl;
import bo.gob.sin.sen.onion.domain.MensajeServicioBuilder;

import java.util.concurrent.CompletableFuture;

public class GuardaChisteServiceImpl extends ApplicationServiceImpl implements IGuardaChisteService {
    IChuckRepository vRespository;
    IChuckService vService;

    public GuardaChisteServiceImpl(ChuckServiceClient chuckJokes, IChuckRepository adicionarChiste) {
        this.vRespository = adicionarChiste;
        this.vService = chuckJokes;
    }

    @Override
    public RespuestaGuardadoChiste guardaElChisteChuck() {
        var vRespuesta = RespuestaGuardadoChiste.builder().transaccion(Boolean.FALSE);
        try{
            var vChiste = vService.obtenerChiste();
            vRespository.adicionarChiste(34, vChiste.getValue(), vChiste.getCreated_at());
            vRespuesta.build().setTransaccion(Boolean.TRUE);
            vRespuesta.build().addMensaje(MensajeServicioBuilder.create().codigo(1).descripcion("Chiste Registrado correctamente").build());

        }catch (Exception ex){
            ex.printStackTrace();
            vRespuesta.build().addMensaje(MensajeServicioBuilder.create().codigo(-1).descripcion("Error al guardar los datos").build());
        }
        return vRespuesta.build();
    }

    @Override
    public CompletableFuture<RespuestaGuardadoChiste> guardaElChisteChuckAsync() {
        return CompletableFuture.supplyAsync(()-> guardaElChisteChuck(), FixedExecutorSingle.instance());
    }
}
