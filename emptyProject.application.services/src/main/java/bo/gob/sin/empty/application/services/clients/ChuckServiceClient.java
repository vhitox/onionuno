package bo.gob.sin.empty.application.services.clients;

import bo.gob.sin.empty.application.IChuckService;
import bo.gob.sin.empty.application.impl.RespuestaCkuck;
import bo.gob.sin.sen.onion.application.service.clients.ClientServiceImpl;

import java.util.concurrent.CompletableFuture;

public class ChuckServiceClient extends ClientServiceImpl implements IChuckService {

    private String urlBase;

    public ChuckServiceClient(){
        this.urlBase = "https://api.chucknorris.io/jokes/random";
    }

    public ChuckServiceClient(String urlBase){
        super();
        this.urlBase = urlBase;
    }


    @Override
    public RespuestaCkuck obtenerChiste() {
        return obtenerChisteAsync().join();
    }

    @Override
    public CompletableFuture<RespuestaCkuck> obtenerChisteAsync() {
        var vUrl = new StringBuilder(urlBase);
        System.out.println("CLiente url: "+urlBase);
        return getAsyncRestClient()
                .getAsync(vUrl.toString(), jsonBuilder.toJson(""))
                .thenApply(vResponse -> {
                   return jsonBuilder.fromJson(vResponse, RespuestaCkuck.class);
                });
    }
}
