package bo.gob.sin.empty.domain.models.entities;

import bo.gob.sin.sen.onion.domain.models.entities.EntidadID;

import java.io.Serializable;
import java.util.Date;

public class Chuck extends EntidadID implements Serializable {
    private String chiste;
    private String dateCreated;

    public String getChiste() {
        return chiste;
    }

    public void setChiste(String chiste) {
        this.chiste = chiste;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public static ChuckBuilder builder() {
        return new ChuckBuilder();
    }

    public static final class ChuckBuilder {
        private Chuck chuck;

        private ChuckBuilder() {
            chuck = new Chuck();
        }

        public ChuckBuilder chiste(String chiste) {
            chuck.setChiste(chiste);
            return this;
        }

        public ChuckBuilder dateCreated(String dateCreated) {
            chuck.setDateCreated(dateCreated);
            return this;
        }

        public ChuckBuilder usuarioRegistroId(Long usuarioRegistroId) {
            chuck.setUsuarioRegistroId(usuarioRegistroId);
            return this;
        }

        public ChuckBuilder usuarioUltimaModificacionId(Long usuarioUltimaModificacionId) {
            chuck.setUsuarioUltimaModificacionId(usuarioUltimaModificacionId);
            return this;
        }

        public ChuckBuilder fechaRegistro(Date fechaRegistro) {
            chuck.setFechaRegistro(fechaRegistro);
            return this;
        }

        public ChuckBuilder fechaUltimaModificacion(Date fechaUltimaModificacion) {
            chuck.setFechaUltimaModificacion(fechaUltimaModificacion);
            return this;
        }

        public ChuckBuilder estadoId(String estadoId) {
            chuck.setEstadoId(estadoId);
            return this;
        }

        public Chuck build() {
            return chuck;
        }
    }
}
