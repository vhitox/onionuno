package bo.gob.sin.empty.domain.respositories;

import bo.gob.sin.empty.domain.models.entities.Chuck;
import bo.gob.sin.sen.onion.domain.RepositoryException;
import bo.gob.sin.sen.onion.domain.repositories.IEntidadIDRepository;

public interface IChuckRepository extends IEntidadIDRepository<Chuck> {

    void adicionarChiste(Integer id, String chiste, String fecha) throws RepositoryException;

}
