package bo.gob.sin.empty.domain.services;

import bo.gob.sin.empty.domain.respositories.IChuckRepository;
import bo.gob.sin.empty.domain.services.repositories.ChuckRepositoryImpl;
import bo.gob.sin.sen.onion.domain.services.postgres.PostgresDataSourceSingle;

public class DomainServiceFactory {
    public static IChuckRepository adicionarChiste(){
        return new ChuckRepositoryImpl(PostgresDataSourceSingle.instance());
    }
}
