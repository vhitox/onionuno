package bo.gob.sin.empty.domain.services.repositories;

import bo.gob.sin.empty.domain.models.entities.Chuck;
import bo.gob.sin.empty.domain.respositories.IChuckRepository;
import bo.gob.sin.sen.onion.domain.RepositoryException;
import bo.gob.sin.sen.onion.domain.services.EntidadIDRepositoryImpl;

import javax.sql.DataSource;
import org.sql2o.Connection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ChuckRepositoryImpl extends EntidadIDRepositoryImpl<Chuck> implements IChuckRepository {

    public ChuckRepositoryImpl(DataSource pDataSource) {
        super(pDataSource);
    }

    @Override
    public void adicionarChiste(Integer id,String chiste, String fecha) throws RepositoryException {
        //var vSelect = new StringBuilder("SELECT * FROM public.chuck");
        var vInsert = new StringBuilder("INSERT INTO public.chuck (id,chiste,date_created) VALUES (:pId,:pChiste,:pFecha)");
        List<Map<String, Object>> vMapList = new ArrayList<>();
        try (Connection vConnection = getSql2o().open()){
            vConnection.createQuery(vInsert.toString())
                    .addParameter("pId",id)
                    .addParameter("pChiste",chiste)
                    .addParameter("pFecha",fecha)
                    .executeUpdate().getKey(Long.class);
            /*vMapList = vConnection.createQuery(vSelect.toString())
                    .executeAndFetchTable().asList();
            System.out.println(vMapList);*/

        }catch (Exception ex){
            throw new RepositoryException("NO GUARDO",ex);
        }
    }

    @Override
    public Chuck findById(Long aLong) throws RepositoryException {
        return null;
    }

    @Override
    protected Chuck mapear(Map<String, Object> map) {
        return null;
    }
}
